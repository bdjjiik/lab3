def print_object_structure():
    print('{ input: { csv, encoding, json, }, output: { encoding, fname }')


def print_help_message():
    print('usage: weather_watcher.py <input_file_name>')


def print_dev_info():
    print('There you need to fill info about the lab`s creator')


def print_init_success(name):
    print(f"ini {name} : OK")


def print_csv_success(name):
    print(f"input-csv {name} : OK")


def print_json_success(name):
    print(f"input-json {name} : OK")


def print_fit_success():
    print('json?=csv: OK')


def print_task_assignment():
    print("Need to fill")
