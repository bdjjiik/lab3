import json
import csv
from src.lexer import *
from src.error import *
from src.info import *


def open_json_file(name, err):
    try:
        with open(name) as file:
            return json.load(file)
    except:
        raise Error(err)


def open_csv_file(name, file_encoding, err):
    try:
        with open(name, encoding=file_encoding) as file:
            return list(csv.reader(file, delimiter=';'))
    except:
        raise Error(err)


def open_to_write(name, file_encoding, err):
    try:
        return open(name, 'w+', encoding=file_encoding)
    except:
        raise Error(err)


class Loader:
    def __init__(self, input_file_name):
        self.input_file_name = input_file_name
        self.input_config = None
        self.output_config = None
        self.count = 0
        self.max_diff = 0
        self.ws_list = []

    def _validate_schema_init(self, config):
        def validate_key(key, data):
            if key not in data:
                raise

        try:
            validate_key('input', config)
            self.input_config = config['input']
            validate_key('csv', self.input_config)
            validate_key('encoding', self.input_config)
            validate_key('json', self.input_config)

            validate_key('output', config)
            self.output_config = config['output']
            validate_key('encoding', self.output_config)
            validate_key('fname', self.output_config)
        except:
            raise Error('init_file_err')

    def load_init(self):
        try:
            self._validate_schema_init(config=open_json_file(self.input_file_name, INIT_FILE_ERR))
        except Error as err:
            print(err)
            print_object_structure()
            return False
        else:
            print_init_success(self.input_file_name)
            return True

    def load_data(self):
        try:
            records = open_csv_file(self.input_config['csv'], self.input_config['encoding'], CSV_READ_ERR)[1:]
            for record in records:
                if record:
                    if

            self.ws_list = [make_weather_station(record) ]
        except Error as err:
            print(err)
            return False
        else:
            print_csv_success(self.input_config['csv'])
            return True

    def load_stat(self):
        try:
            data = [v for v in open_json_file(self.input_config['json'], JSON_READ_ERR).values()]

            if len(data) < 2:
                raise Error(JSON_INCORRECT_ERR)

            self.max_diff = data[0]
            self.count = data[1]

        except Error as err:
            print(err)
            return False
        else:
            print_json_success(self.input_config['json'])
            return True

    def fit(self):
        try:
            count = len(self.ws_list)
            max_diff = max(ws.get_daily_difference() for ws in self.ws_list)

            if count != self.count or max_diff != self.max_diff:
                raise Error(FIT_ERR)

        except Error as err:
            print(err)
            return False
        else:
            print_fit_success()
            return True

    def load(self):
        if self.load_init() and self.load_data() and self.load_stat() and self.fit():
            return self.ws_list
        else:
            return []

    def output(self, data):
        if self.output_config['fname']:
            try:
                file = open_to_write(self.output_config['fname'], self.output_config['encoding'], OUTPUT_ERR)
                file.write('\n'.join(data))
                file.close()
            except Error as err:
                print(err)
        else:
            for line in data:
                print(line)
