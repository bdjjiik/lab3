import operator


class WeatherWatcherDB:
    def __init__(self, ws_list):
        self.db = ws_list
        for ws in self.db:
            ws.observe = self.count_observes(ws.meteo)

    def count_observes(self, meteo):
        return sum(meteo == ws.meteo for ws in self.db)

    def get_max_avgt(self, count):
        if self.db:
            ws_list = sorted(self.db, key=lambda ws: ws.avgt, reverse=True)[:count]
            return sorted(ws_list, key=operator.attrgetter('hum', 'observe', 'meteo'))
        else:
            return []

    def get_meteo_observe(self, meteo):
        if self.db:
            return sorted([ws for ws in self.db if ws.meteo == meteo], key=operator.attrgetter('hum', 'yyyy', 'mm', 'dd'))
        else:
            return []

