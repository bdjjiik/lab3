class WeatherStation:
    """
    A class used to represent an WeatherStation
    ...

    Attributes
    ----------
    meteo : str
        the code of the weather station
    observations : Observations
        the observations which meteo station made

    Methods
    -------
    says(sound=None)
        Prints the animals name and what sound it makes
    """
    def __init__(self, meteo):
        self.meteo = meteo
        self.observations = []

    def add_observation(self, observation):
        self.observations.append(observation)

    def get_daily_difference(self):
        print(1)
        # return self.maxt - self.mint

