class Observation:
    def __init__(self, rain, yyyy, mint, maxt, mm, dd, wind, hum, avgt):
        self.dd = int(dd)
        self.yyyy = int(yyyy)
        self.mm = int(mm)
        self.avgt = float(avgt)
        self.mint = float(mint)
        self.maxt = float(maxt)
        self.hum = float(hum)
        self.rain = int(rain)
        self.wind = float(wind)
