def format_error(message):
    return '***** {} *****'.format(message)


class Error(Exception):
    def __init__(self):
        self.message = None

    def __str__(self):
        return '{}\n{}'.format(format_error('program aborted'), format_error(self.message))


class CommandLineError(Error):
    def __init__(self):
        self.message = 'command line error'


class CsvIncorrectError(Error):
    def __init__(self):
        self.message = 'incorrect input csv-file'

INIT_FILE_ERR = 'init file error'
CSV_READ_ERR = 'can not read input csv-file'
CSV_INCORRECT_ERR = 'incorrect input csv-file'
JSON_READ_ERR = 'can not read input csv-file'
JSON_INCORRECT_ERR = 'incorrect input json-file'
FIT_ERR = 'inconsistent information'
OUTPUT_ERR = 'can not write output file'
