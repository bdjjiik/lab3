import re
from src.error import CsvIncorrectError
from src.observation import Observation


def validate_meteo(meteo_code):
    """ Validate meteo-code
        <meteo-code> := <digit><digit-string> | <letter><digit-string>
        <digit-string> := <digit><digit-string> | <letter><digit-string> | <empty>
        <digit> := 1 ... 9
        <string> := A ... Z, a ... z
        { length 3 .. 7 }

        :parameter
        ----------
        meteo_code : str

        :raises
        -------
        Csv Incorrect Error
    """
    if not re.match('^\\w{3,7}$', meteo_code):
        raise CsvIncorrectError


def validate_numb(numb, length):
    """ Validate number
        <number> := <digit><digit-string>
        <digit-string> := <digit><digit-string>  | <empty>
        <digit> := 1 ... 9

        :parameter
        ----------
        numb : str
        length : str
            represent maximum length of numb

        :raises
        -------
        Csv Incorrect Error
    """
    if not re.match(r'^\d{{1,{}}}$'.format(length), numb):
        raise CsvIncorrectError


def validate_numb_fixed(numb, length):
    """ Validate number with fixed length
        <number> := <digit><digit-string>
        <digit-string> := <digit><digit-string>  | <empty>
        <digit> := 1 ... 9

        :parameter
        ----------
        numb : str
        length : str
            represent exactly length which numb must fit

        :raises
        -------
        Csv Incorrect Error
    """
    if not re.match(r'^\d{{{}}}$'.format(length), numb):
        raise CsvIncorrectError


def validate_numb_with_point(numb, precision):
    """ Validate float number with fixed precision
        <number> := <sign><decimal><fractional>
        <decimal> := <digit><digit-string>
        <fractional> := .<digit><digit-string>
        <sign> := - | + | <empty>
        <digit-string> := <digit><digit-string> | <empty>
        <digit> := 1 ... 9

        :parameter
        ----------
        numb : str
        precision : str
            represent numbers` precision

        :raises
        -------
        Csv Incorrect Error
    """
    if not re.match(r'^([+-]?)\d+\.\d{{{}}}$'.format(precision), numb):
        raise CsvIncorrectError


def validate_date(yyyy, mm, dd):
    """ Validate Date
        Year start from January.
        Counts month from zero: 0 January, 11 December
        months in the year := 12
        days in month := 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 (accordingly)

        :parameter
        ----------
        dd : str
        yyyy : str
        mm : str

        :raises
        -------
        Csv Incorrect Error
    """
    try:
        months_in_year = 12
        days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

        # Validate date (short-day, short-month, year) format
        yyyy_len = 4  # {<yyyy> length = 4}
        mm_len = 2  # {<mm>   length = 2}
        dd_len = 2  # {<dd>   length = 2}
        validate_numb_fixed(yyyy, yyyy_len)
        validate_numb(dd, dd_len)
        validate_numb(mm, mm_len)

        # Check current year is leap if yes add one day in 1 February
        if not int(yyyy) % 4:
            days_in_month[1] += 1

        # Check correct date
        if int(mm) > months_in_year or int(dd) > days_in_month[int(mm) - 1]:
            raise CsvIncorrectError
    except:
        raise


def validate_fields(fields):
    """ Validate fields to scheme specified in the task
        fields order
        0 - rain
        1 - yyyy
        2 - mint
        3 - meteo
        4 - maxt
        5 - mm
        6 - dd
        7 - wind
        8 - hum
        9 - avgt

        :parameter
        ----------
        fields : list
            contains information about record


        :raises
        -------
        Csv Incorrect Error
    """
    try:
        max_digit = 3  # Validate rain (from 0 to 100 percent)
        validate_numb(fields[0], max_digit)

        # Validate date
        validate_date(fields[1], fields[5], fields[6])

        # Validate meteo
        validate_meteo(fields[3])

        temp_precision = 1  # Validate temperature (1 digit after point)
        validate_numb_with_point(fields[2], temp_precision)
        validate_numb_with_point(fields[4], temp_precision)
        validate_numb_with_point(fields[9], temp_precision)

        wind_precision = 2  # Validate wind (2 digit after point)
        validate_numb_with_point(fields[7], wind_precision)

        hum_precision = 1  # Validate wind (1 digit after point)
        validate_numb_with_point(fields[8], hum_precision)
    except:
        raise CsvIncorrectError


def build(fields):
    """ Build from fields exemplar of Observation
        fields order
        0 - rain
        1 - yyyy
        2 - mint
        3 - meteo
        4 - maxt
        5 - mm
        6 - dd
        7 - wind
        8 - hum
        9 - avgt

        :parameter
        ----------
        fields : list
            contains information about record

        :return
        -------
        List format [meteo-code, Observation]

        :raises
        -------
        Error incorrect input csv-file
    """

    try:
        return [fields[3], Observation(fields[0], fields[1], fields[2], fields[4], fields[5], fields[6], fields[7],
                                       fields[8], fields[9])]
    except:
        raise CsvIncorrectError
