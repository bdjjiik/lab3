import sys
from src.error import *
from src.loader import Loader
from src.info import *
from src.weather_station_db import WeatherWatcherDB


def parse_args():
    input_file_name_pos = 1
    total_args_count = 2
    """
     Keyword Arguments:
          0 prog -- The name of the program (default: sys.argv[0]);
          1 input_file_name -- A name of input file in cur directory (type=str)

          total_args_count = 2;
    """

    args = sys.argv
    try:
        if len(args) is not total_args_count:
            raise CommandLineError
    except Error as err:
        print(err)
        print_help_message()
        return ''
    else:
        return args[input_file_name_pos]


def main_task():
    file_name = parse_args()
    if file_name:
        loader = Loader(file_name)
        ws_db = WeatherWatcherDB(ws_list=loader.load())
        data = []
        for ws in ws_db.get_max_avgt(4):
            data.append(f'{ws.observe}\t{round(ws.avgt, 1)}\t{ws.hum}\t{ws.meteo}')
            for same_ws in ws_db.get_meteo_observe(ws.meteo):
                data.append(f'\t{ws.avgt}\t{round(ws.maxt, 1)}\t{ws.mint}\t{ws.wind}\t{ws.hum}\t{ws.mm}\t{ws.dd}\t{ws.yyyy}')
        loader.output(data)


if __name__ == "__main__":
    print_dev_info()
    print_task_assignment()

    main_task()
